### how to use:

i believe you already know the basics of how to open the console or developer tools on your pc, so i will be very straightforward.

open steam in your browser, go to the friends tab, then open the developer tools or console of your browser.
each browser has a different way to open it, but the quickest is to right-click anywhere, click on inspect, a page will open, and you just need to click on console. after opening the console, you copy the code from the "mass script" file, paste it into the console, and a box will open for you to write what you want to post on your steam friends' profiles and below to select all, some, or none.

that's it.

don't mess it up!